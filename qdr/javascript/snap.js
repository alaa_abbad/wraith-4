// Sometimes, straggling requests never make it back, in which
// case, timeout after 5 seconds and render the page anyway
final_timeout = setTimeout(function() {
console.log('Snapping ' + url + ' at width ' + view_port_width);
page.render(image_name);
phantom.exit();
}, 10000);

